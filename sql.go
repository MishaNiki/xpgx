package xpgx

import (
	"context"
	"database/sql"
	"database/sql/driver"
	"github.com/jackc/pgx/v4/stdlib"
	"sync"
)

var (
	xpgxDriver *Driver
	muLock = &sync.Mutex{}
	//chLock = make(chan struct{}, 2)
)

type Driver struct {
	pgxDriver driver.Driver
}

type Connector struct {
	driver driver.Driver
	name string

	pgxConnector driver.Connector
}

type Conn struct {
	pgxConn driver.Conn

	pgxExecerContext driver.ExecerContext
	pgxQueryerContext driver.QueryerContext
	pgxConnPrepareContext driver.ConnPrepareContext
	pgxConnBeginTx driver.ConnBeginTx
}

type Tx struct {
	conn *Conn

	pgxTx driver.Tx
}

func init() {
	xpgxDriver = &Driver{
		pgxDriver: stdlib.GetDefaultDriver(),
	}
	sql.Register("xpgx", xpgxDriver)
	//chLock <- struct{}{}
	//chLock <- struct{}{}
}

func (d *Driver) Open(name string) (driver.Conn, error) {
	pgxConn, err := d.pgxDriver.Open(name)
	if err != nil {
		return nil, err
	}
	conn := &Conn{
		pgxConn: pgxConn,
	}
	if ec, ok := pgxConn.(driver.ExecerContext);  ok {
		conn.pgxExecerContext = ec
	}
	if qc, ok := pgxConn.(driver.QueryerContext);  ok {
		conn.pgxQueryerContext = qc
	}
	if cpc, ok := pgxConn.(driver.ConnPrepareContext);  ok {
		conn.pgxConnPrepareContext = cpc
	}
	if cbt, ok := pgxConn.(driver.ConnBeginTx);  ok {
		conn.pgxConnBeginTx = cbt
	}
	return conn, nil
}


func (c *Conn) Prepare(query string) (driver.Stmt, error) {
	return c.pgxConn.Prepare(query)
}

func (c *Conn) Close()  error {
	return c.pgxConn.Close()
}


func (d *Driver) OpenConnector(name string) (driver.Connector, error) {
	dc, ok := d.pgxDriver.(driver.DriverContext)
	if !ok {
		panic("BLYAT!!!")
	}
	pgxConnector, err := dc.OpenConnector(name)
	if err != nil {
		return nil, err
	}
	return &Connector{driver: d, pgxConnector: pgxConnector, name: name}, nil
}

func (c *Connector) Connect(ctx context.Context) (driver.Conn, error) {
	pgxConn, err := c.pgxConnector.Connect(ctx)
	if err != nil {
		return nil, err
	}
	conn := &Conn{ pgxConn: pgxConn}
	if ec, ok := pgxConn.(driver.ExecerContext);  ok {
		conn.pgxExecerContext = ec
	}
	if qc, ok := pgxConn.(driver.QueryerContext);  ok {
		conn.pgxQueryerContext = qc
	}
	if cpc, ok := pgxConn.(driver.ConnPrepareContext);  ok {
		conn.pgxConnPrepareContext = cpc
	}
	if cbt, ok := pgxConn.(driver.ConnBeginTx);  ok {
		conn.pgxConnBeginTx = cbt
	}
	return conn, nil
}

func (c *Connector) Driver() driver.Driver {
	return c.driver
}


func (c *Conn) Begin() (driver.Tx, error) {
	var (
		err error
		pgxTx driver.Tx
	)
	if c.pgxConnBeginTx != nil {
		pgxTx, err = c.pgxConnBeginTx.BeginTx(context.Background(), driver.TxOptions{})
	} else {
		pgxTx, err = c.pgxConn.Begin()
	}
	if err != nil {
		return nil, err
	}
	tx := &Tx{ conn: c, pgxTx: pgxTx}
	return tx, nil
}


func (c *Conn) ExecContext(ctx context.Context, query string, args []driver.NamedValue) (driver.Result, error) {
	return c.pgxExecerContext.ExecContext(ctx, query, args)
}

func (c *Conn) QueryContext(ctx context.Context, query string, args []driver.NamedValue) (driver.Rows, error) {
	return c.pgxQueryerContext.QueryContext(ctx, query, args)
}

func (c *Conn) PrepareContext(ctx context.Context, query string) (driver.Stmt, error) {
	return c.pgxConnPrepareContext.PrepareContext(ctx, query)
}


func (c *Conn) BeginTx(ctx context.Context, opts driver.TxOptions) (driver.Tx, error) {
	muLock.Lock()
	//<- chLock
	pgxTx, err := c.pgxConnBeginTx.BeginTx(ctx, opts)
	if err != nil {
		return nil, err
	}
	return &Tx{
		conn: c,
		pgxTx: pgxTx,
	}, nil
}


func (tx *Tx) Commit() error {
	defer muLock.Unlock()
	//defer func () {chLock <- struct{}{}}()
	return tx.pgxTx.Commit()
}

func (tx *Tx) Rollback() error {
	defer muLock.Unlock()
	//defer func () {chLock <- struct{}{}}()
	return tx.pgxTx.Rollback()
}
