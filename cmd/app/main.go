package main

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/jmoiron/sqlx"
	"log"
	"math/rand"
	"sync"
	"time"

	_ "gitlab.com/MishaNiki/xpgx"
)

const numberOfGoroutines = 10

func main() {
	//	urlExample := "postgres://username:password@localhost:5432/database_name"
	urlExample := "postgres://xpgx_user:xpgx_pass@localhost:5885/xpgx_test"
	conn, err := sqlx.Connect("xpgx", urlExample)
	if err != nil {
		logErr(err)
		log.Fatal(err)
	}
	//conn.SetMaxOpenConns(1)
	//conn.SetMaxIdleConns(1)
	wg := sync.WaitGroup{}
	ctx := context.Background()
	for i := 0; i < numberOfGoroutines; i ++ {
		wg.Add(1)
		go runTx(ctx, conn, &wg, i)
	}
	wg.Wait()
}

const (
	minSleepSecond = 1
	maxSleepSecond = 4

	minNumberOfQueries = 2
	maxNumberOfQueries = 8
)


func runTx(ctx context.Context, db *sqlx.DB, wg *sync.WaitGroup, idx int) {
	defer wg.Done()
	logInfo(fmt.Sprintf("Gorutines #%d: is waiting for the start of the transaction", idx))
	tx, err := db.BeginTxx(ctx, &sql.TxOptions{})
	if err != nil {
		logErr(err)
		return
	}
	logInfo(fmt.Sprintf("Gorutines #%d: transaction started", idx))
	if err := startLongQueries(ctx, tx, idx); err != nil {
		logErr(err)
		return
	}
	if err := tx.Commit(); err != nil {
		logErr(err)
		return
	}
	logInfo(fmt.Sprintf("Gorutines #%d: transaction finish", idx))
}

func startLongQueries(ctx context.Context, tx *sqlx.Tx, idx int) error {
	numberOfQueries := RandIntInRange(minNumberOfQueries, maxNumberOfQueries)
	sleepSecond := RandIntInRange(minSleepSecond, maxSleepSecond)
	query := `SELECT pg_sleep($1);`
	for i := 0; i < numberOfQueries; i++ {
		if _, err := tx.ExecContext(ctx, query, sleepSecond); err != nil {
			return err
		}
		logInfo(fmt.Sprintf("the query: %s completed successfully. sleep second: %d. i = %d, goruntine number = %d", query, sleepSecond, i, idx))
	}
	return nil
}


func RandIntInRange(a, b int)  int{
	rand.Seed(time.Now().UnixNano())
	return rand.Int() % (b - a + 1) + a
}


func logErr(err error) {
	log.Printf("\u001b[1m\u001b[31mERROR:\u001b[39m\u001b[0m %v\n", err)
}

func logInfo(msg string) {
	log.Printf("\u001b[1m\u001b[102mINFO:\u001b[39m\u001b[0m %s\n", msg)
}