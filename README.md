# xpgx

Custom driver for PostgreSQL 

```bash
docker run -d \
--name xpgx_db \
-p 5885:5432 \
-e POSTGRES_USER=xpgx_user \
-e POSTGRES_DB=xpgx_test \
-e POSTGRES_PASSWORD=xpgx_pass \
postgres:12.6
```
