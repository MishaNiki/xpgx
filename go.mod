module gitlab.com/MishaNiki/xpgx

go 1.16

require (
	github.com/jackc/pgx/v4 v4.12.0 // indirect
	github.com/jmoiron/sqlx v1.3.4 // indirect
)
